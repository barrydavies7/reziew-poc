package com.reziew.poc.ml;

import java.util.*;

import org.junit.jupiter.api.*;

import weka.classifiers.functions.*;
import weka.core.*;

public class BarryLearninMachineLearning {

  private static final int trainingSetSize = 50;

  private Attribute            sizeAttr;
  private Attribute            sqrdSizeAttr;
  private Attribute            priceAttr;
  private ArrayList<Attribute> attrs;
  private Instances            trainingDataset;
  private LinearRegression     targetFunction;

  @Test
  public void tryOutWeka() throws Exception {
    buildDimensions();
    buildTrainingSetWithDimensions();
    buildLinearRegressionClassifier();
    Instances predictionSet = new Instances("predictionSet", attrs, 1);
    predictionSet.setClassIndex(trainingDataset.numAttributes() - 1);
    predictionSet.add(buildInstance(60, priceAttr));
    predictionSet.add(buildInstance(60, sqrdSizeAttr));
    predictionSet.add(buildInstance(60, sizeAttr));
    double pricePrediction = targetFunction.classifyInstance(predictionSet.get(0));
    double sqrdPrediction = targetFunction.classifyInstance(predictionSet.get(1));
    double sizePrediction = targetFunction.classifyInstance(predictionSet.get(2));
    System.out.println(
        String.format(
            "Predictions:  price [%s]; sqrd [%s]; size [%s];",
            pricePrediction,
            sqrdPrediction,
            sizePrediction));
  }

  private void buildLinearRegressionClassifier() throws Exception {
    this.targetFunction = new LinearRegression();
    targetFunction.buildClassifier(trainingDataset);
  }

  private void buildTrainingSetWithDimensions() {
    this.attrs = new ArrayList<>(Arrays.asList(sizeAttr, sqrdSizeAttr, priceAttr));
    this.trainingDataset = new Instances("trainingData", attrs, trainingSetSize);
    trainingDataset.setClassIndex(trainingDataset.numAttributes() - 1);
    for (int i = 0; i < trainingSetSize; i++)
      trainingDataset.add(buildInstance(i, null));
  }

  private void buildDimensions() {
    this.sizeAttr = new Attribute("sizeFeature");
    this.sqrdSizeAttr = new Attribute("squaredSizeFeature");
    this.priceAttr = new Attribute("priceLabel");
  }

  private Instance buildInstance(double unsquared, Attribute toOmit) {
    double price = unsquared * 2.5;
    double squared = Math.pow(unsquared, 2);
    Instance instance = new DenseInstance(3);
    if (!sizeAttr.equals(toOmit))
      instance.setValue(sizeAttr, unsquared);
    if (!sqrdSizeAttr.equals(toOmit))
      instance.setValue(sqrdSizeAttr, squared);
    if (!priceAttr.equals(toOmit))
      instance.setValue(priceAttr, price);
    if (toOmit == null)
      System.out.println(String.format("Instance:  price [%s]; sqrd [%s]; size [%s];", price, squared, unsquared));

    return instance;
  }
}
